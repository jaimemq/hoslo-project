# HOSLO! Project #

HOSLO! Project or simply HOSLO! is an Android health mobile application oriented to the organisation of the long-term patient’s time and interconnection of those patients.

### Summary ###

HOSLO! Project contains four different sections:

* Audiovisual Resources: In this section, the user can find Movies, Music and Books. We have used resources from Internet Archive ([https://archive.org/](Link URL)), a non-profit library of free books, movies...

* Forum: Place to share questions, advices, concers... with other users, who can answer the different publications.

* Encyclopedia: A selection of the most relevant Norwegian health websites where reliable information about diseases can be found.

* Calendar: Calendar view that allows the user to set the days when they are available to receive visitors and share it via e-mail, WhatsApp, Facebook or any other social network installed in their smartphone.

### Bugs ###

* [ ] Navigation Drawer. Navigation drawer needs to be re-considerated. Problems while showing the different fragments.

* [ ] Forum. MOST SIGNIFICANT BUG. To make HOSLO! Project completely functional, we should upload topics and comments to a cloud server (in order to inter-connect all the users). I had problems to understand how it works (PHP language), so I wasn not able to get it. SOLVING THIS, I CONSIDER THE APP COULD BE READY, IN SPITE OF THE OTHER (LESS) SIGNIFICANT BUGS.

* [ ] In Calendar: Sort selected days when they are shared.

### Future Improvements ###

* Thinking about a better way to show free resources. Section Music needs a most comfortable interface.

* Synchronize the calendar view with Google Calendar. In this way, users can check their personal calendar/events and according to that, select the days (and hours) to receive visitors that fit the best with their schedule. It was impossible for me to get this goal. There are not too many resources online about this.

* Synchronization with Google Calendar is the main goal, but in second place, maybe easier to get, a future improvement could be to share the calendar view instead of only the days.


# Android #

HOSLO! is only developed for Android mobile phones (Android 4.0.3 and +).

# Other technical considerations #

### Programming language ###

Android (Java, XML).

### Supported languages ###

English, Norwegian, Spanish.

### Repository link ###

[https://jaimemq@bitbucket.org/jaimemq/hoslo-project.git](Link URL) [Public]