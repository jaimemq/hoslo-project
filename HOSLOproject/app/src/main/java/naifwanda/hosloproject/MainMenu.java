package naifwanda.hosloproject;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import naifwanda.hosloproject.Audiovisual.AudiovisualMenu;
import naifwanda.hosloproject.Calendar.CalendarActivity;
import naifwanda.hosloproject.Drawer.DrawerItem;
import naifwanda.hosloproject.Drawer.DrawerListAdapter;
import naifwanda.hosloproject.Forum.ForumActivity;

public class MainMenu extends AppCompatActivity implements View.OnClickListener{

    //DrawerLayout declarations
    private String[] mTagTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private MyActionBarDrawerToggle mDrawerToggle;


    //Buttons declaration
    CardView audiovisualButton;
    CardView forumButton;
    CardView encyclopediaButton;
    CardView calendarButton;
    CardView settingsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //To delete Action Bar, but appears in XML yet https://www.youtube.com/watch?v=eXpjGOEQktA
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //
        setContentView(R.layout.activity_main_menu);
        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);


        /*
        //Font Awesome by Dave Gandy - http://fontawesome.io
        //Code taken from: http://stackoverflow.com/questions/15210548/how-to-use-a-icons-and-symbols-from-font-awesome-on-native-android-application
        Typeface font = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );
        Button button = (Button)findViewById( R.id.button_audiovisual );
        button.setTypeface(font);
        */


        //Set Up UI
        drawerLayoutSetUp(this, toolbar);
        buttonSetUp();


    }
    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Set up for Drawer Layout and derivatives
     * version 13.user1.2017
     */
    public void drawerLayoutSetUp(final Activity activity, Toolbar toolbar){

        //mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


        //Obtain strings of titles
        mTagTitles = getResources().getStringArray(R.array.tags_array);
        //Obtain drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        //Obtain listview
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        //To include a header in the list view
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.header_drawer, mDrawerList, false);
        mDrawerList.addHeaderView(header, null, false);
        //New list of drawer items
        ArrayList<DrawerItem> items = new ArrayList<DrawerItem>();
        items.add(new DrawerItem(mTagTitles[0],R.drawable.ic_home_black_24dp));
        items.add(new DrawerItem(mTagTitles[1],R.drawable.ic_local_movies_black_24dp));
        items.add(new DrawerItem(mTagTitles[2],R.drawable.ic_forum_black_24dp));
        items.add(new DrawerItem(mTagTitles[3],R.drawable.ic_account_balance_black_24dp));
        items.add(new DrawerItem(mTagTitles[4],R.drawable.ic_date_range_black_24dp));

        //Link the drawer layout with the adapter
        mDrawerList.setAdapter(new DrawerListAdapter(this, items));

        //Code from http://stackoverflow.com/questions/10295226/how-to-create-listview-onitemclicklistener
        mDrawerList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 1:

                        Log.i("MainMenu", "To Audiovisual");

                        Intent intentToMainMenu = new Intent(activity, MainMenu.class);
                        startActivity(intentToMainMenu);


                        break;
                    case 2:
                        Log.i("MainMenu", "To Audiovisual");

                        Intent intentToAudiovisual = new Intent(activity, AudiovisualMenu.class);
                        startActivity(intentToAudiovisual);
                        break;
                    case 3:
                        Log.i("MainMenu", "To Forum");

                        Intent intentToForum = new Intent(activity, ForumActivity.class);
                        startActivity(intentToForum);
                        break;
                    case 4:
                        Log.i("MainMenu", "To Encyclopedia");

                        Intent intentToEncyclopedia = new Intent(activity, EncyclopediaActivity.class);
                        startActivity(intentToEncyclopedia);
                        break;
                    case 5:
                        Log.i("MainMenu", "To Calendar");

                        Intent intentToCalendar = new Intent(activity, CalendarActivity.class);
                        startActivity(intentToCalendar);
                        break;

                    default:
                        break;
                }
            }
        });
        mDrawerToggle = new MyActionBarDrawerToggle(activity, mDrawerLayout,toolbar,R.string.drawer_open, R.string.drawer_close);


    }

    /**
     * Set up for all the buttons
     * version 13.user1.2017
     */
    public void buttonSetUp(){
        audiovisualButton = (CardView) findViewById(R.id.card_view_audiovisual);
        forumButton = (CardView) findViewById(R.id.card_view_forum);
        encyclopediaButton = (CardView) findViewById(R.id.card_view_encyclopedia);
        calendarButton = (CardView) findViewById(R.id.card_view_calendar);
        //settingsButton = (Button)findViewById(R.id.button_settings);

        //Listener set up
        audiovisualButton.setOnClickListener(this);
        forumButton.setOnClickListener(this);
        encyclopediaButton.setOnClickListener(this);
        calendarButton.setOnClickListener(this);
        //settingsButton.setOnClickListener(this);


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // I think there's a bug in mToggle.onOptionsItemSelected, because it always returns false.
        // The item id testing is a fix.
        if (mDrawerToggle.onOptionsItemSelected(item) || item.getItemId() == android.R.id.home) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * In charge of the buttons actions
     * version 13.user1.2017
     */

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.card_view_audiovisual:
                Log.i("MainMenu", "To Audiovisual");

                Intent intentToAudiovisual = new Intent(this, AudiovisualMenu.class);
                startActivity(intentToAudiovisual);
                break;
            case R.id.card_view_forum:
                Log.i("MainMenu", "To Forum");

                Intent intentToForum = new Intent(this, ForumActivity.class);
                startActivity(intentToForum);
                break;
            case R.id.card_view_encyclopedia:
                Log.i("MainMenu", "To Encyclopedia");

                Intent intentToEncyclopedia = new Intent(this, EncyclopediaActivity.class);
                startActivity(intentToEncyclopedia);
                break;
            case R.id.card_view_calendar:
                Log.i("MainMenu", "To Calendar");

                Intent intentToCalendar = new Intent(this, CalendarActivity.class);
                startActivity(intentToCalendar);
                break;
            /*
            case R.id.button_settings:
                Log.i("MainMenu", "To Settings");

                Intent intentToSettings = new Intent(this, AudiovisualMenu.class);
                startActivity(intentToSettings);
                break;

                */
            default:
                break;
        }

    }

    /**
     * Class to show the "hamburger" icon
     * Code taken from: http://stackoverflow.com/questions/30370886/how-to-set-drawer-toggle-icon-hamburger-for-the-actionbardrawertoggle-v7-wit
     */
    public class MyActionBarDrawerToggle extends android.support.v7.app.ActionBarDrawerToggle {

        public MyActionBarDrawerToggle(Activity activity, final DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);

            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            setDrawerIndicatorEnabled(false);

            setToolbarNavigationClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            });
        }
    }
}
