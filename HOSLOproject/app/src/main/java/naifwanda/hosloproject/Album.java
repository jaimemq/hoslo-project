package naifwanda.hosloproject;

/**
 *
 * Code taken from https://www.youtube.com/watch?v=7Fe1jigV5Qs
 * Created by jaime on 24/01/2017.
 */

public class Album {
    //Audiovisual sections
    private String name;
    private String numOfSongs;
    private int thumbnail;
    private int year;
    private String info;
    private String url;

    //Forum
    private String date;
    private String title;
    private int userPicture;
    private String userName;
    private String text;
    private String answerNumber;


    public Album() {
    }
    //Constructor for audiovisual sections
    public Album(String name, String numOfSongs, int thumbnail, String info, String url){
        this.name = name;
        this.numOfSongs = numOfSongs;
        this.thumbnail = thumbnail;
        this.info = info;
        this.url = url;
    }
    //Constructor for forum
    public Album(String date, String title, int userPicture, String userName, String text, String answerNumber){
        this.date = date;
        this.title = title;
        this.userPicture = userPicture;
        this.userName = userName;
        this.text = text;
        this.answerNumber = answerNumber;

    }

    //Getters and Setters of the FORUM
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(int userPicture) {
        this.userPicture = userPicture;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnswerNumber() {
        return answerNumber;
    }

    public void setAnswerNumber(String answerNumber) {
        this.answerNumber = answerNumber;
    }

    //Getters and Setters of the AUDIOVISUAL SECTIONS

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumOfSongs() {
        return numOfSongs;
    }

    public void setNumOfSongs(String numOfSongs) {
        this.numOfSongs = numOfSongs;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
