package naifwanda.hosloproject.NotUsed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import naifwanda.hosloproject.R;


/**
 *
 * ExpandableList code: https://www.youtube.com/watch?v=GD_U0-N3zUI
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MoviesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class MoviesFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    ExpandableListView expandableListView;
    Button buttonVisualize;

    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view;
        view = inflater.inflate(R.layout.fragment_movies, container);

        expandableListSetUp(view);
        buttonVisualize = (Button)view.findViewById(R.id.button_visualize);
        buttonVisualize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToUrl ( "https://archive.org/details/charlie_chaplin_film_fest#");
            }
        });



        return view;
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_movies, container, false);
    }

    /**
     * method to go to a specific url
     * Code taken from: http://stackoverflow.com/questions/5026349/how-to-open-a-website-when-a-button-is-clicked-in-android-application
     * @param url
     */
    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }


    /**
     * Expandable list view set up
     * @param view
     */
    public void expandableListSetUp(View view){

        expandableListView = (ExpandableListView) view.findViewById(R.id.movies_exp_listview);

        List<String> headings = new ArrayList<String>();
        List<String> H1 = new ArrayList<String>();

        HashMap<String, List<String>> childList = new HashMap<String, List<String>>();
        String headingItems[] = getResources().getStringArray(R.array.header_titles);
        String h1Items[] = getResources().getStringArray(R.array.h1_items);

        for(String title : headingItems){
            headings.add(title);
        }
        for(String title : h1Items){
            H1.add(title);
        }

        childList.put(headings.get(0), H1);

        ExpandableListAdapter myAdapter = new ExpandableListAdapter(this.getActivity(), headings, childList);

        expandableListView.setAdapter(myAdapter);

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
           // throw new RuntimeException(context.toString()
                   // + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
