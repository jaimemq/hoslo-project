package naifwanda.hosloproject.Drawer;

/**
 * Items of the Drawer Layout
 *
 * Code taken from: http://www.hermosaprogramacion.com/2014/11/android-navigation-drawer-tutorial/
 *
 * Created by jaime on 10/01/2017.
 */

public class DrawerItem {
    private String name;
    private int iconId;

    public DrawerItem(String name, int iconId) {
        this.name = name;
        this.iconId = iconId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}