package naifwanda.hosloproject.Calendar;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import naifwanda.hosloproject.MainMenu;
import naifwanda.hosloproject.R;

/**
 * Code from: https://www.youtube.com/watch?v=RN4Zmxlah_I
 * (2)Code from: https://github.com/prolificinteractive/material-calendarview
 * (3)SQLite code from:  https://www.youtube.com/watch?v=KUq5wf3Mh0c&index=4&list=PLS1QulWo1RIaRdy16cOzBO5Jr6kEagA07
 */
public class CalendarActivity extends AppCompatActivity{

    MaterialCalendarView materialCalendarView;
    Calendar currentDate;
    CalendarDay selectedDate;
    CardView buttonVisitors;
    CardView buttonReturn;
    CardView buttonClear;

    List<CalendarDay> listSelectedDates;
    List<CalendarDay> listStoredDates;

    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);

        //Back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myDb = new DatabaseHelper(this);

        listSelectedDates = new ArrayList<CalendarDay>();
        listStoredDates = new ArrayList<CalendarDay>();



        buttonVisitors = (CardView) findViewById(R.id.btn_card_view_calendar);
        buttonReturn = (CardView) findViewById(R.id.btn_card_view_calendar2);
        buttonClear = (CardView) findViewById(R.id.btn_card_view_calendar3);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialCalendarView.clearSelection();
            }
        });
        buttonVisitors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //buttonVisitors.setBackgroundColor(Color.RED);
                materialCalendarView.clearSelection();
                buttonVisitors.setVisibility(View.INVISIBLE);
                buttonReturn.setVisibility(View.VISIBLE);
                buttonClear.setVisibility(View.VISIBLE);
                materialCalendarView.setSelectionColor(Color.parseColor("#00ac0b"));
                materialCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);

            }
        });
        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Decorator code taken from: http://stackoverflow.com/questions/33512727/material-calendarview-set-background-color-of-dates
                listSelectedDates = materialCalendarView.getSelectedDates();
                for (int i = 0; i < listSelectedDates.size(); i++){
                    //Add data to database
                    addData(listSelectedDates.get(i));
                }
                if (listStoredDates == null){
                    listStoredDates = listSelectedDates;
                }else{
                    if (listSelectedDates != null) {
                        listStoredDates.addAll(listSelectedDates);
                    }
                }

                materialCalendarView.addDecorators(new EventDecorator(Color.parseColor("#00ac0b"), listSelectedDates));


                buttonVisitors.setVisibility(View.VISIBLE);
                buttonReturn.setVisibility(View.INVISIBLE);
                materialCalendarView.setSelectionColor(Color.parseColor("#c72b2b"));
                materialCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
            }
        });

        currentDate = Calendar.getInstance();
        selectedDate =  null;

        materialCalendarView = (MaterialCalendarView)findViewById(R.id.calendarView);
        materialCalendarView.setSelectedDate(currentDate);
        materialCalendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(1900, 0, 1))
                .setMaximumDate(CalendarDay.from(2200, 11, 31))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();
        /*
        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                selectedDate = date;
                int month = date.getMonth() + 1;
                Toast.makeText(CalendarActivity.this, ""+ date.getYear()+"/" +month +
                            "/"+ date.getDay(), Toast.LENGTH_SHORT).show();
            }

        });
        */

        //Show the dates in the database with the Event Decorator
        showAll();


    }
    public void viewAll(){
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            Toast.makeText(CalendarActivity.this, "Empty", Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            buffer.append("Id :"+ res.getString(0)+"\n");
            buffer.append("Year:"+ res.getInt(1)+"\n");
            buffer.append("Month :"+ res.getInt(2)+"\n");
            buffer.append("Day :"+ res.getInt(3)+"\n\n");
        }

        // Show all data
        Toast.makeText(CalendarActivity.this, buffer.toString(), Toast.LENGTH_LONG).show();
    }

    //Method to show all the items in the database
    public void showAll(){
        Cursor res = myDb.getAllData();
        if (res.getCount() != 0){
            while (res.moveToNext()){
                CalendarDay date = new CalendarDay(res.getInt(1), res.getInt(2), res.getInt(3));
                listStoredDates.add(date);
            }
        }
        materialCalendarView.addDecorators(new EventDecorator(Color.parseColor("#00ac0b"), listStoredDates));

    }
    //Method to add data in the database
    public void addData(CalendarDay date){
        myDb.insertData(date);
        /*boolean isInserted = myDb.insertData(date);
        if(isInserted == true)
            Toast.makeText(CalendarActivity.this,"Data Inserted",Toast.LENGTH_LONG).show();
        else
            Toast.makeText(CalendarActivity.this,"Data not Inserted",Toast.LENGTH_LONG).show();
*/
    }

    /**
     * Class to stablish the decoration of selected items
     * Code taken from http://stackoverflow.com/questions/33512727/material-calendarview-set-background-color-of-dates
     */
    private class EventDecorator implements DayViewDecorator {

        private final int color;
        private final HashSet<CalendarDay> dates;

        public EventDecorator(int color, Collection<CalendarDay> dates) {
            this.color = color;
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new DotSpan(7, color));
        }
    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                Intent intent = new Intent(CalendarActivity.this, MainMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            case R.id.trash_icon:
                Log.i("MainMenu", "Size: "+listStoredDates.size());
                if (listStoredDates.size() != 0){
                    new AlertDialog.Builder(this)
                            .setMessage(getResources().getString(R.string.delete_entry_question))
                            .setNegativeButton(R.string.keep, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton(R.string.txt_delete, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    dialog.dismiss();
                                    materialCalendarView.removeDecorators();
                                    listStoredDates = new ArrayList<CalendarDay>();
                                    myDb.deleteAll();
                                }
                            })
                            .show();
                }else{
                    Toast.makeText(CalendarActivity.this, getResources().getString(R.string.no_marked_days), Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.screen_shot:
                share();
                /*
                final LinearLayout layout = (LinearLayout)findViewById(R.id.activity_calendar);
                layout.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("onOptions", " to takeScreenShot");

                        Bitmap pic = takeScreenShot(layout);
                        try {
                            if(pic != null){
                                Log.i("onOptions", " to saveScreenShot");

                                saveScreenShot(pic);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                */
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void share(){
        Log.i("onOptions", listStoredDates.toString());
        if(listStoredDates.size() != 0){
            ArrayList<String> newStyleDates = new ArrayList<String>();
            for (int i = 0; i<listStoredDates.size();i++){
                int year = listStoredDates.get(i).getYear();
                int month = listStoredDates.get(i).getMonth() + 1;
                int day = listStoredDates.get(i).getDay();
                String date = year + "/" + month + "/" + day;
                newStyleDates.add(i, date);
                Log.i("onOptions", newStyleDates + "\n");

            }
            StringBuilder sb = new StringBuilder();
            for (String s : newStyleDates)
            {
                sb.append(s);
                sb.append("\n");
            }
            //Share with whatsapp.
            //Code from: http://stackoverflow.com/questions/12952865/how-to-share-text-to-whatsapp-from-my-app
            //Code (2) from: https://www.whatsapp.com/faq/es/android/28000012
            //Code (3) from: https://javaheros.blogspot.no/2015/10/compartir-en-redes-sociales-en-android.html
            String helloTemplate = "Hello! You alright? If you are receiving this message is because I would like you to visit me at hospital. " +
                    "It would make me happy :). These are the days I'm available to receive visitors: ";
            String hosloTemplate = "This is a message generated by HOSLO! Project, developed by Naifwanda";
        /*
        //This code only for an specific app (change "com.whatsapp" for any other app)
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, helloTemplate + "\n\n" + sb.toString() + "\n" + hosloTemplate);
        try {
            this.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(CalendarActivity.this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
        }
        */
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT,  helloTemplate + "\n\n" + sb.toString() + "\n" + hosloTemplate);
            startActivity(Intent.createChooser(intent, getResources().getString(R.string.share)));
        }else{
            Toast.makeText(CalendarActivity.this, getResources().getString(R.string.select_days), Toast.LENGTH_SHORT).show();
        }
    }

/*
    private Bitmap takeScreenShot(View v){
        Log.i("onOptions", " takeScreenShot");

        Bitmap screenShot = null;

        try{
            int width = v.getMeasuredWidth();
            int heigth = v.getMeasuredHeight();

            screenShot = Bitmap.createBitmap(width, heigth, Bitmap.Config.ARGB_8888);

            Canvas c = new Canvas(screenShot);
            v.draw(c);

        }catch (Exception e){
            Log.i("onOptions", " exception");

            e.printStackTrace();
        }
        return screenShot;
    }
    private void saveScreenShot(Bitmap bm){
        Log.i("onOptions", " saveScreenShot");
        verifyStoragePermissions(this);
        ByteArrayOutputStream bao = null;
        File file = null;

        try{
            bao = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, bao);

            //Write as a file to SD card
            file = new File(Environment.getExternalStorageDirectory()+File.separator+"hola");
            if(!file.exists()){
                Log.i("onOptions", " File doesn't exist");

                file.createNewFile();
            }

            FileOutputStream fos = new FileOutputStream(file, false);
            fos.write(bao.toByteArray());
            fos.close();
        }catch (Exception e){
            Log.i("onOptions", " exception2");

            e.printStackTrace();
        }
    }
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
*/
    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
   /* public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
*/

}
