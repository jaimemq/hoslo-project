package naifwanda.hosloproject;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import naifwanda.hosloproject.Card.CardInfoActivity;

/**
 * Code taken from https://www.youtube.com/watch?v=7Fe1jigV5Qs
 * OnClickListener implementation from: https://www.youtube.com/watch?v=xEHHdpxW7iA
 *
 * Created by jaime on 24/01/2017.
 */


public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.MyViewHolder> {

        private Context mContext;
        private ArrayList<Album> albumList;

        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public TextView title, count, year, info;
            public ImageView thumbnail, overflow;
            ArrayList<Album> albumArrayList = new ArrayList<Album>();
            Context mContext;


            public MyViewHolder(View view, Context mContext, ArrayList<Album> albumArrayList) {
                super(view);
                this.mContext = mContext;
                this.albumArrayList = albumArrayList;
                view.setOnClickListener(this);
                title = (TextView) view.findViewById(R.id.title);
                count = (TextView) view.findViewById(R.id.count);
                thumbnail = (ImageView) view.findViewById(R.id.thumbnail);

            }

            @Override
            public void onClick(View view) {
                    int position = getAdapterPosition();
                    Album album = this.albumArrayList.get(position);

                    Intent intentToCardInfo = new Intent(this.mContext, CardInfoActivity.class);
                    //Intent intentToWeb = new Intent(this.mContext, WebViewActivity.class);

                    intentToCardInfo.putExtra("title", album.getName());
                    intentToCardInfo.putExtra("image", album.getThumbnail());
                    intentToCardInfo.putExtra("count", album.getNumOfSongs());
                    intentToCardInfo.putExtra("info", album.getInfo());

                    intentToCardInfo.putExtra("url", album.getUrl());

                    this.mContext.startActivity(intentToCardInfo);
            }
        }


        public AlbumsAdapter(Context mContext, ArrayList<Album> albumList) {
            this.mContext = mContext;
            this.albumList = albumList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_custom, parent, false);

            return new MyViewHolder(itemView, mContext, albumList);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            Album album = albumList.get(position);
            holder.title.setText(album.getName());
            holder.count.setText(album.getNumOfSongs());

            // loading album cover using Glide library
            Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);
/*
            holder.overflow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopupMenu(holder.overflow);
                }
            });
            */
        }

        /**
         * Showing popup menu when tapping on 3 dots
         */
        private void showPopupMenu(View view) {
            // inflate menu

        }

        /**
         * Click listener for popup menu items
         */
        class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

            public MyMenuItemClickListener() {
            }

            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                return false;
            }
        }

        @Override
        public int getItemCount() {
            return albumList.size();
        }
}

