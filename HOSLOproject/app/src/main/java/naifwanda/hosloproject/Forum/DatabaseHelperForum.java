package naifwanda.hosloproject.Forum;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import naifwanda.hosloproject.Album;

/**
 * Code from: https://www.youtube.com/watch?v=KUq5wf3Mh0c&index=4&list=PLS1QulWo1RIaRdy16cOzBO5Jr6kEagA07
 * Created by jaime on 27/02/2017.
 */

public class DatabaseHelperForum extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Topics.db";
    public static final String TABLE_NAME = "topics_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "DATE";
    public static final String COL_3 = "TITLE";
    public static final String COL_4 = "USER";
    public static final String COL_5 = "USERIMAGE";
    public static final String COL_6 = "TEXT";
    public static final String COL_7 = "ANSWERS";



    public DatabaseHelperForum(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "DATE DATE,TITLE TEXT,USER TEXT, USERIMAGE INTEGER, TEXT TEXT, ANSWERS INTEGER)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(Album album){
        return insertData(album.getDate(), album.getTitle(), album.getUserName(), album.getUserPicture(), album.getText(), album.getAnswerNumber());
    }
    public boolean insertData(String date, String title, String user, int userImage, String text, String answers) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, date);
        contentValues.put(COL_3, title);
        contentValues.put(COL_4, user);
        contentValues.put(COL_5, userImage);
        contentValues.put(COL_6, text);
        contentValues.put(COL_7, answers);


        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        return res;
    }

    public boolean updateData(String id, int answers) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_7, answers);
        db.update(TABLE_NAME, contentValues, "ID = ?", new String[]{id});
        return true;
    }

    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new String[]{id});
    }
    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);    }
}