package naifwanda.hosloproject.Forum;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import naifwanda.hosloproject.Album;
import naifwanda.hosloproject.R;
import naifwanda.hosloproject.Spinner.SpinnerAdapter;

public class TopicInfoActivity extends AppCompatActivity {

    TextView title;
    TextView name;
    TextView date;
    TextView text;
    TextView answer;
    ImageView image;
    TextView comment;
    TextView answersLabel;

    String answerNumber;
    int commentsCounter = 0;

    //RecyclerView
    private RecyclerView recyclerView;
    private ForumAdapter adapter;
    private ArrayList<Album> albumList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_info);
        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);

        //Back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        //Recycler view
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_ti);
        albumList = new ArrayList<>();
        adapter = new ForumAdapter(this, albumList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        title = (TextView)findViewById(R.id.txt_title);
        name = (TextView)findViewById(R.id.txt_user_name);
        date = (TextView)findViewById(R.id.txt_date);
        text = (TextView)findViewById(R.id.txt_text);
        image = (ImageView)findViewById(R.id.img_user);
        answer = (TextView)findViewById(R.id.answers_ti);
        answersLabel = (TextView)findViewById(R.id.txt_answer);

        text.setMovementMethod(new ScrollingMovementMethod());

        title.setText(getIntent().getStringExtra("title"));
        date.setText(getIntent().getStringExtra("date"));
        name.setText(getIntent().getStringExtra("name"));
        text.setText(getIntent().getStringExtra("text"));
        answerNumber = getIntent().getStringExtra("answers");
        answer.setText(getResources().getString(R.string.no_answer));
        answersLabel.setText(getResources().getString(R.string.no_answer));


        image.setImageResource(getIntent().getIntExtra("picture", 00));

        comment = (TextView) findViewById(R.id.comment_ti);

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAlertDialog();
                adapter.notifyDataSetChanged();
            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(TopicInfoActivity.this, ForumActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("answers", answerNumber);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method to display an alert dialog for creating a new topic
     *
     * Code taken from: https://www.youtube.com/watch?v=plnLs6aST1M
     * About adjusting to screen size: http://stackoverflow.com/questions/2306503/how-to-make-an-alert-dialog-fill-90-of-screen-size
     *
     */
    void setAlertDialog(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(TopicInfoActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.pop_up_new_topic, null);
        ImageButton buttonCross = (ImageButton) mView.findViewById(R.id.cross);
        TextView newTopic = (TextView) mView.findViewById(R.id.txt_new_topic);
        newTopic.setText(getResources().getString(R.string.add_comment));
        TextView done = (TextView) mView.findViewById(R.id.txt_done);
        TextView title = (TextView) mView.findViewById(R.id.txt_set_title);
        final EditText setTitle = (EditText)mView.findViewById(R.id.set_title);
        TextView name = (TextView) mView.findViewById(R.id.txt_set_author);
        final EditText setAuthor = (EditText)mView.findViewById(R.id.set_author);
        TextView avatar = (TextView) mView.findViewById(R.id.txt_set_avatar);
        final Spinner spinnerAvatar = (Spinner)mView.findViewById(R.id.spinner);
        final EditText setText = (EditText)mView.findViewById(R.id.set_msg);

        // Spinner with images: http://stackoverflow.com/questions/3609231/how-is-it-possible-to-create-a-spinner-with-images-instead-of-text
        SpinnerAdapter adapter = new SpinnerAdapter(this,
                new Integer[]{R.drawable.user1,R.drawable.user2,R.drawable.user3,R.drawable.user4,
                        R.drawable.user5,R.drawable.user6,R.drawable.user7,R.drawable.user8,
                        R.drawable.user9,R.drawable.user10,R.drawable.user11,R.drawable.user12,
                        R.drawable.user13,R.drawable.user14,R.drawable.user15,R.drawable.user16,});
        spinnerAvatar.setAdapter(adapter);
        mBuilder.setView(mView);
        //Adjusting screen
        final AlertDialog dialog = mBuilder.create();

        buttonCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //code taken from: http://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android
                new AlertDialog.Builder(TopicInfoActivity.this)
                        .setMessage(getResources().getString(R.string.delete_entry_question))
                        .setPositiveButton(R.string.abandon, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog1, int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.keep, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog1, int which) {
                                // do nothing
                                dialog1.cancel();

                            }
                        })
                        .show();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!setTitle.getText().toString().isEmpty() && !setAuthor.getText().toString().isEmpty()
                        && !setText.getText().toString().isEmpty()) {

                    commentsCounter++;
                    if(commentsCounter == 1){
                        answer.setText(commentsCounter + " " + getResources().getString(R.string.answer1));
                        answersLabel.setText(commentsCounter + " " + getResources().getString(R.string.answer1));
                    }else{
                        answer.setText(commentsCounter + " " + getResources().getString(R.string.answer));
                        answersLabel.setText(commentsCounter + " " + getResources().getString(R.string.answer));
                    }

                    String stringTitle = setTitle.getText().toString();
                    String stringAuthor = setAuthor.getText().toString();
                    String stringText = setText.getText().toString();
                    int intImg = spinnerAvatar.getSelectedItemPosition();
                    if(intImg == 0){
                        intImg = R.drawable.user1;
                    }else if (intImg == 1){
                        intImg = R.drawable.user2;
                    }else if (intImg == 2){
                        intImg = R.drawable.user3;
                    }else if (intImg == 3){
                        intImg = R.drawable.user4;
                    }else if (intImg == 4){
                        intImg = R.drawable.user5;
                    }else if (intImg == 5){
                        intImg = R.drawable.user6;
                    }else if (intImg == 6){
                        intImg = R.drawable.user7;
                    }else if (intImg == 7){
                        intImg = R.drawable.user8;
                    }else if (intImg == 8){
                        intImg = R.drawable.user9;
                    }else if (intImg == 9){
                        intImg = R.drawable.user10;
                    }else if (intImg == 10){
                        intImg = R.drawable.user11;
                    }else if (intImg == 11){
                        intImg = R.drawable.user12;
                    }else if (intImg == 12){
                        intImg = R.drawable.user13;
                    }else if (intImg == 13){
                        intImg = R.drawable.user14;
                    }else if (intImg == 14){
                        intImg = R.drawable.user15;
                    }else if (intImg == 15){
                        intImg = R.drawable.user16;
                    }

                    String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43
                    Album a = new Album(date, stringTitle, intImg, stringAuthor, stringText, getResources().getString(R.string.txt_comment) + " #"+ commentsCounter);
                    albumList.add(a);

                    answerNumber = answerNumber + 1;

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_comment_added), Toast.LENGTH_SHORT).show();

                    dialog.dismiss();
                }else{
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.fulfill), Toast.LENGTH_SHORT).show();
                }  }
        });

        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(dialog.getWindow().getAttributes());
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(params);

    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            //int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing -  spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = spacing / spanCount; // (column + user1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - spacing / spanCount; // spacing - (column + user1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}
