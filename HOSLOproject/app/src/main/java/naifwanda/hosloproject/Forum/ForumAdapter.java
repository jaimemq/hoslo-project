package naifwanda.hosloproject.Forum;

/**
 * Created by jaime on 02/02/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import naifwanda.hosloproject.Album;
import naifwanda.hosloproject.R;

/**
 * Code taken from https://www.youtube.com/watch?v=7Fe1jigV5Qs
 * OnClickListener implementation from: https://www.youtube.com/watch?v=xEHHdpxW7iA
 *
 * Created by jaime on 2/2/2017.
 */


public class ForumAdapter extends RecyclerView.Adapter<ForumAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Album> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView date, title, userName, text, readMore, answers, comment;
        public ImageView userPicture;
        ArrayList<Album> albumArrayList = new ArrayList<Album>();
        Context mContext;


        public MyViewHolder(View view, Context mContext, ArrayList<Album> albumArrayList) {
            super(view);
            this.mContext = mContext;
            this.albumArrayList = albumArrayList;
            view.setOnClickListener(this);

            title = (TextView) view.findViewById(R.id.title_f);
            date = (TextView) view.findViewById(R.id.date);
            userName = (TextView) view.findViewById(R.id.user_name);
            text = (TextView) view.findViewById(R.id.text);
            if(mContext instanceof ForumActivity){
                text.setMaxLines(3);
            }
            //readMore = (TextView) view.findViewById(R.id.read_more);
            answers = (TextView) view.findViewById(R.id.answers);
            //comment = (TextView) view.findViewById(R.id.comment);

            userPicture = (ImageView) view.findViewById(R.id.user_picture);

        }

        @Override
        public void onClick(View view) {

            if(mContext instanceof ForumActivity){
                int position = getAdapterPosition();
                Album album = this.albumArrayList.get(position);


                Intent intentToTopicInfo = new Intent(this.mContext,TopicInfoActivity.class);
                //Intent intentToWeb = new Intent(this.mContext, WebViewActivity.class);

                intentToTopicInfo.putExtra("title", album.getTitle());
                intentToTopicInfo.putExtra("date", album.getDate());
                intentToTopicInfo.putExtra("name", album.getUserName());
                intentToTopicInfo.putExtra("text", album.getText());
                intentToTopicInfo.putExtra("answers", album.getAnswerNumber());
                intentToTopicInfo.putExtra("picture", album.getUserPicture());



                this.mContext.startActivity(intentToTopicInfo);
            }


        }
    }


    public ForumAdapter(Context mContext, ArrayList<Album> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_forum_custom, parent, false);

        return new MyViewHolder(itemView, mContext, albumList);
    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Album album = albumList.get(position);
        holder.title.setText(album.getTitle());
        holder.date.setText(album.getDate());
        holder.userName.setText(album.getUserName());
        holder.text.setText(album.getText());
        holder.answers.setText(album.getAnswerNumber());

        // loading album cover using Glide library
        Glide.with(mContext).load(album.getUserPicture()).into(holder.userPicture);


    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu

    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            return false;
        }
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
