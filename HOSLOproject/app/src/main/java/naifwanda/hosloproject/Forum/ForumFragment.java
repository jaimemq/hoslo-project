package naifwanda.hosloproject.Forum;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import naifwanda.hosloproject.Album;
import naifwanda.hosloproject.R;
import naifwanda.hosloproject.Spinner.SpinnerAdapter;

/**
 * Fragment to manage events in Forum
 * Created by jaime on 01/02/2017.
 */

public class ForumFragment extends Fragment implements View.OnClickListener{
    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private ForumAdapter adapter;
    private ArrayList<Album> albumList;
    private FloatingActionButton buttonNewTopic;

    //SQLite
    DatabaseHelperForum myDb;



    public ForumFragment(){
        //Required empty constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view;
        view = inflater.inflate(R.layout.fragment_forum, container);

        //myDb = new DatabaseHelperForum(getContext());


        //User Interface set up
        buttonNewTopic = (FloatingActionButton)view.findViewById(R.id.fab);

        //To get the current date
        String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43

        //Recycler view
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_forum);
        albumList = new ArrayList<>();
        adapter = new ForumAdapter(getActivity(), albumList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        //Code for hiding FAB when scrolling taken from: http://stackoverflow.com/questions/33208613/hide-floatingactionbutton-on-scroll-of-recyclerview
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy > 0 ||dy<0 && buttonNewTopic.isShown())
                {
                    buttonNewTopic.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    buttonNewTopic.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        buttonNewTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAlertDialog(getActivity());
                adapter.notifyDataSetChanged();

            }
        });


        //Update the topics
        prepareCards();

        //Show stored topics in SQLite
        //viewAll();
        //showAll();


        return view;
    }

    /**
     * Method to display an alert dialog for creating a new topic
     *
     * Code taken from: https://www.youtube.com/watch?v=plnLs6aST1M
     * About adjusting to screen size: http://stackoverflow.com/questions/2306503/how-to-make-an-alert-dialog-fill-90-of-screen-size
     *
     * @param mContext
     */
    void setAlertDialog(final Context mContext){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = getActivity().getLayoutInflater().inflate(R.layout.pop_up_new_topic, null);
        ImageButton buttonCross = (ImageButton) mView.findViewById(R.id.cross);
        TextView newTopic = (TextView) mView.findViewById(R.id.txt_new_topic);
        TextView done = (TextView) mView.findViewById(R.id.txt_done);
        TextView title = (TextView) mView.findViewById(R.id.txt_set_title);
        final EditText setTitle = (EditText)mView.findViewById(R.id.set_title);
        TextView name = (TextView) mView.findViewById(R.id.txt_set_author);
        final EditText setAuthor = (EditText)mView.findViewById(R.id.set_author);
        TextView avatar = (TextView) mView.findViewById(R.id.txt_set_avatar);
        final Spinner spinnerAvatar = (Spinner)mView.findViewById(R.id.spinner);
        final EditText setText = (EditText)mView.findViewById(R.id.set_msg);

        // Spinner with images: http://stackoverflow.com/questions/3609231/how-is-it-possible-to-create-a-spinner-with-images-instead-of-text
        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),
                new Integer[]{R.drawable.user1,R.drawable.user2,R.drawable.user3,R.drawable.user4,
                        R.drawable.user5,R.drawable.user6,R.drawable.user7,R.drawable.user8,
                        R.drawable.user9,R.drawable.user10,R.drawable.user11,R.drawable.user12,
                        R.drawable.user13,R.drawable.user14,R.drawable.user15,R.drawable.user16,});
        spinnerAvatar.setAdapter(adapter);

        mBuilder.setView(mView);
        //Adjusting screen
        final AlertDialog dialog = mBuilder.create();

        buttonCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //code taken from: http://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android
                new AlertDialog.Builder(mContext)
                        .setMessage(getResources().getString(R.string.delete_entry_question))
                        .setPositiveButton(R.string.abandon, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog1, int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.keep, new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog1, int which) {
                                 // do nothing
                                dialog1.cancel();

                             }
                        })
                        .show();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!setTitle.getText().toString().isEmpty() && !setAuthor.getText().toString().isEmpty()
                        && !setText.getText().toString().isEmpty()) {

                    String stringTitle = setTitle.getText().toString();
                    String stringAuthor = setAuthor.getText().toString();
                    String stringText = setText.getText().toString();
                    int intImg = spinnerAvatar.getSelectedItemPosition();
                    if(intImg == 0){
                        intImg = R.drawable.user1;
                    }else if (intImg == 1){
                        intImg = R.drawable.user2;
                    }else if (intImg == 2){
                        intImg = R.drawable.user3;
                    }else if (intImg == 3){
                        intImg = R.drawable.user4;
                    }else if (intImg == 4){
                        intImg = R.drawable.user5;
                    }else if (intImg == 5){
                        intImg = R.drawable.user6;
                    }else if (intImg == 6){
                        intImg = R.drawable.user7;
                    }else if (intImg == 7){
                        intImg = R.drawable.user8;
                    }else if (intImg == 8){
                        intImg = R.drawable.user9;
                    }else if (intImg == 9){
                        intImg = R.drawable.user10;
                    }else if (intImg == 10){
                        intImg = R.drawable.user11;
                    }else if (intImg == 11){
                        intImg = R.drawable.user12;
                    }else if (intImg == 12){
                        intImg = R.drawable.user13;
                    }else if (intImg == 13){
                        intImg = R.drawable.user14;
                    }else if (intImg == 14){
                        intImg = R.drawable.user15;
                    }else if (intImg == 15){
                        intImg = R.drawable.user16;
                    }

                    String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43
                    Album a = new Album(date, stringTitle, intImg, stringAuthor, stringText, 0 + " " + getResources().getString(R.string.answer));
                    albumList.add(a);

                    //Add data to SQLite database
                    //addData(a);
                    Toast.makeText(getActivity(), getResources().getString(R.string.txt_topic_added), Toast.LENGTH_SHORT).show();


                    dialog.dismiss();
                }else{
                    Toast.makeText(getActivity(), getResources().getString(R.string.fulfill), Toast.LENGTH_SHORT).show();
                }
            }
        });
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.copyFrom(dialog.getWindow().getAttributes());
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(params);

    }



    private void prepareCards() {
        int[] covers = new int[]{
                R.drawable.user1,
                R.drawable.user2,
                R.drawable.user3,
                R.drawable.user4



        };
        //To get the current date
        String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43

        String title = "Lorem Ipsum";

        String userName = "Jaime Moreno";

        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Fusce in hendrerit dui. Donec accumsan congue fermentum. Donec " +
                "placerat tortor sed tortor vestibulum, at imperdiet dolor tristique. P" +
                "hasellus ut urna eget leo vehicula ultricies ac a purus. Nulla efficitur" +
                " ligula id mi gravida, nec fringilla risus ornare. Nunc id vulputate urna." +
                " Sed mauris lectus, eleifend at urna nec, pellentesque convallis tortor. " +
                "Donec tincidunt, mi in vestibulum ullamcorper, sem eros cursus odio, nec" +
                " blandit tellus orci eget dui. Curabitur ac accumsan nibh. Aliquam nisi " +
                "lectus, porttitor a tellus vel, eleifend dignissim lorem. Duis rutrum " +
                "dictum diam eget sagittis. In vel auctor velit, ut elementum metus. " +
                "Sed ultrices, enim at pretium faucibus, felis enim venenatis justo, " +
                "vel condimentum lacus massa et purus. Curabitur id mauris ipsum. " +
                "Nulla in nibh mattis, pharetra tortor at, elementum urna. " +
                "Etiam aliquam ullamcorper ipsum, ut cursus tellus convallis vitae.\n" +
                "\n" +
                "\n" +
                "Pellentesque mattis eros velit, a placerat nisi condimentum iaculis. Nulla tristique fringilla semper. Nunc a laoreet ipsum. In ultrices, orci in commodo pulvinar, dui odio mollis metus, vitae gravida nunc lacus quis nisl. Nunc sodales, orci et facilisis fermentum, erat arcu fringilla neque, porta cursus massa orci et ex. Nullam aliquam sapien non lacus pretium, non fermentum velit hendrerit. Nam eget sapien eu lorem dictum blandit. Ut a nulla justo. Phasellus malesuada sagittis neque, ut sollicitudin ligula pulvinar non. Phasellus dictum metus in ex porta malesuada. Cras nec mollis ante.";

        String answers = "0";

        Album a = new Album(date, title, covers[0], userName, text, answers + " " + getResources().getString(R.string.answer));
        albumList.add(a);



        //To get the current date
        date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43

        title = "\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...\"\n";

        userName = "Jaime Moreno";

        text = " \"Integer tempus interdum dapibus. Curabitur convallis diam at facilisis " +
                "aliquam. Aenean interdum elit sed fermentum fermentum. Phasellus tellus" +
                " nunc, scelerisque ac tincidunt vitae, maximus et felis. Proin auctor ex " +
                "et tristique tempor. Sed eget erat facilisis, pellentesque nisi in, " +
                "sollicitudin tellus. Vivamus auctor accumsan orci. Class aptent taciti " +
                "sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos." +
                " Phasellus quam purus, gravida quis lacinia nec, dapibus ac ipsum. " +
                "Mauris vitae lectus urna. Vestibulum eget venenatis odio. Donec gravida " +
                "neque eu augue consectetur sodales. Nulla egestas leo sit amet turpis placerat," +
                " a lobortis mi viverra. Donec vehicula vitae nisi ut consectetur. " +
                "Ut in ipsum dignissim, convallis tellus ut, placerat purus.\\n\" +\n";

        answers = "0";

        a = new Album(date, title, covers[1], userName, text, answers +" " + getResources().getString(R.string.answer));
        albumList.add(a);



        //To get the current date
        date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43

        title = "HOSLO! Project";

        userName = "Jaime Moreno";

        text = "\"Donec a augue nec nisl commodo tempus in malesuada urna. Donec quam turpis," +
                " vulputate vulputate consequat sit amet, condimentum vel diam. Aenean venenatis" +
                " turpis leo, ac bibendum arcu facilisis et. Mauris rhoncus auctor mauris. " +
                "Aliquam auctor, leo vitae facilisis blandit, nunc nulla aliquam massa, vel" +
                " efficitur ex lacus eu libero. Vivamus eget ultrices arcu, ut scelerisque " +
                "lorem. Nullam imperdiet ultrices metus, sit amet egestas lectus pulvinar non. " +
                "Aliquam finibus ullamcorper nisi, " +
                "vel tempus sem rhoncus ut. Duis mi dolor, iaculis id scelerisque eu," +
                " ornare vitae ex. Cras eu accumsan felis, posuere convallis lectus.\\n\" +\n";

        answers = "0";

        a = new Album(date, title, covers[2], userName, text, answers + " " +getResources().getString(R.string.answer));
        albumList.add(a);



        //To get the current date
        date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()); //2016/11/16 12:08:43

        title = "Bachelor Thesis";

        userName = "Jaime Moreno";

        text = " \"Morbi eu nulla vitae dui viverra pulvinar. Sed eget justo id dolor " +
                "porttitor malesuada. Sed viverra est feugiat est commodo consequat. " +
                "Etiam dignissim porta nisl vel gravida. Donec ut urna molestie, lacinia" +
                " eros non, bibendum urna. In id massa non ante rhoncus euismod. " +
                "Duis pulvinar nisl sed metus volutpat, sit amet mattis lacus pretium. " +
                "Nulla id sem ut leo ornare volutpat. Cras vitae felis aliquam, porttitor " +
                "erat in, rhoncus ante. Sed non tellus nec arcu vulputate porta sit amet " +
                "nec odio. Curabitur consectetur ultricies tellus ut elementum. Cum sociis" +
                " natoque penatibus et magnis dis parturient " +
                ", nascetur ridiculus mus. Sed tortor leo, tincidunt non mauris placerat, " +
                "ullamcorper molestie nunc. Nam blandit non lorem sit amet sollicitudin.\\n\" +\n";

        answers = "0";

        a = new Album(date, title, covers[3], userName, text, answers + " " + getResources().getString(R.string.answer));
        albumList.add(a);

        adapter.notifyDataSetChanged();
    }

    /**
    * In charge of the buttons actions
    * version 13.user1.2017
    */
    @Override
    public void onClick(View view) {
       switch (view.getId()) {


           default:
               break;
       }
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            //int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing -  spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = spacing / spanCount; // (column + user1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - spacing / spanCount; // spacing - (column + user1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    //Method to add data in the database
    public void addData(Album album){
        myDb.insertData(album);
        /*boolean isInserted = myDb.insertData(album);
        if(isInserted == true)
            Toast.makeText(getActivity(),"Data Inserted",Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getActivity(),"Data not Inserted",Toast.LENGTH_LONG).show();
*/
    }

    public void viewAll(){
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            Toast.makeText(getActivity(), "Empty", Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            buffer.append("Id :"+ res.getString(0)+"\n");
            buffer.append("Date:"+ res.getString(1)+"\n");
            buffer.append("Title :"+ res.getString(2)+"\n");
            buffer.append("User :"+ res.getString(3)+"\n");
            buffer.append("UserImage :"+ res.getInt(4)+"\n");
            buffer.append("Text :"+ res.getString(5)+"\n");
            buffer.append("Answers :"+ res.getInt(6)+"\n\n");

        }

        // Show all data
        Toast.makeText(getActivity(), buffer.toString(), Toast.LENGTH_LONG).show();
    }

    //Method to show all the items in the database
    public void showAll(){
        Cursor res = myDb.getAllData();
        if (res.getCount() != 0){
            while (res.moveToNext()){
                Album album = new Album(res.getString(1), res.getString(2), res.getInt(4), res.getString(3),res.getString(5), res.getString(6));
                albumList.add(album);
                adapter.notifyDataSetChanged();            }
        }

    }


    // TODO: Rename method, update argument and hook method into UI event
        public void onButtonPressed(Uri uri) {
            if (mListener != null) {
                mListener.onFragmentInteraction(uri);
            }
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnFragmentInteractionListener) {
                mListener = (OnFragmentInteractionListener) context;
            } else {
                RuntimeException e = new RuntimeException();
                try {
                    throw e;
                } catch (RuntimeException e1) {
                    e1.printStackTrace();
                }
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }



        /**
         * This interface must be implemented by activities that contain this
         * fragment to allow an interaction in this fragment to be communicated
         * to the activity and potentially other fragments contained in that
         * activity.
         * <p>
         * See the Android Training lesson <a href=
         * "http://developer.android.com/training/basics/fragments/communicating.html"
         * >Communicating with Other Fragments</a> for more information.
         */
        public interface OnFragmentInteractionListener {
            // TODO: Update argument type and name
            void onFragmentInteraction(Uri uri);
        }



}
