package naifwanda.hosloproject.Audiovisual;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import naifwanda.hosloproject.NotUsed.MoviesFragment;
import naifwanda.hosloproject.R;

/**
 * Code taken from: https://www.youtube.com/watch?v=K3HlCvlNkbI
 * Created by jaime on 17/01/2017.
 */

public class AudiovisualMenuFragment extends Fragment implements View.OnClickListener{
    private OnFragmentInteractionListener mListener;


    //Buttons
    CardView moviesButton;
    CardView musicButton;
    CardView booksButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view;
        view = inflater.inflate(R.layout.fragment_audiovisual_section, container);
            buttonSetUp(view);






            return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }
    /**
     * Set up for all the buttons
     * version 13.user1.2017
     */
    public void buttonSetUp(View view){
        moviesButton = (CardView) view.findViewById(R.id.card_view_movies);
        musicButton = (CardView) view.findViewById(R.id.card_view_music);
        booksButton = (CardView) view.findViewById(R.id.card_view_books);

        //Listener set up

        moviesButton.setOnClickListener(this);
        musicButton.setOnClickListener(this);
        booksButton.setOnClickListener(this);


    }
    /**
     * In charge of the buttons actions
     * version 13.user1.2017
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_view_movies:
                Log.i("AudiovisualMenu", "To Movies");

                Intent intentToMovies = new Intent(this.getActivity(), MoviesActivity.class);
                startActivity(intentToMovies);
                break;
            case R.id.card_view_music:
                Log.i("AudiovisualMenu", "To Music");
/*
                //fragment administrator
                FragmentManager mManager = getFragmentManager();

                FragmentTransaction mTransaction = mManager.beginTransaction();
                MusicCardFragment musicCardFragment = new MusicCardFragment();
                mTransaction.add(R.id.layout_audiovisual , musicCardFragment);
                mTransaction.commit();
                */
                Intent intentToMusic = new Intent(this.getActivity(), MusicActivity.class);
                startActivity(intentToMusic);
                break;
            case R.id.card_view_books:
                Log.i("AudiovisualMenu", "To Books");

                Intent intentToBooks = new Intent(this.getActivity(), BooksActivity.class);
                startActivity(intentToBooks);
                break;
            default:
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MoviesFragment.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            RuntimeException e = new RuntimeException();
            try {
                throw e;
            } catch (RuntimeException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
