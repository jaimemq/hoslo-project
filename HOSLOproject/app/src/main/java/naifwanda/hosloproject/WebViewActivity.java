package naifwanda.hosloproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Activity to create the web view of every url
 * Code taken from: https://www.youtube.com/watch?v=sr5dr9A2De8
 */
public class WebViewActivity extends AppCompatActivity {

    WebView web;
    String receivedUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);

        //Back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        web = (WebView) findViewById(R.id.web_view);
        WebSettings settings= web.getSettings();
        settings.setJavaScriptEnabled(true);
        receivedUrl = (String) getIntent().getStringExtra("url");

        web.setWebViewClient(new MyWebViewClient());
        web.loadUrl(receivedUrl);

    }

    private class MyWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                //PROBLEM HERE WHEN I TRY TO COME BACK. CARDINFOACTIVITY IS A CLASS USED BY SEVERAL
                //aCTIVITIES. HOW CAN I CHECK FROM WHAT ACTIVITY I'M COMING? solved
                super.onBackPressed();
                /*Intent intent = new Intent(WebViewActivity.this, CardInfoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                */
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
