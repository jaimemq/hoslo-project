package naifwanda.hosloproject.Card;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import naifwanda.hosloproject.NotUsed.MoviesFragment;
import naifwanda.hosloproject.R;

/**
 *
 * Created by jaime on 24/01/2017.
 */

public class CardInfoFragment extends Fragment {
    private CardInfoFragment.OnFragmentInteractionListener mListener;
    TextView info;
    ImageView cardImage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view;
        view = inflater.inflate(R.layout.card_information, container);





        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MoviesFragment.OnFragmentInteractionListener) {
            mListener = (CardInfoFragment.OnFragmentInteractionListener) context;
        } else {
            RuntimeException e = new RuntimeException();
            try {
                throw e;
            } catch (RuntimeException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
