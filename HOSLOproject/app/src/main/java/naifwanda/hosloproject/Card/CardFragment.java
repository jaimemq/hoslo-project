package naifwanda.hosloproject.Card;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import naifwanda.hosloproject.Album;
import naifwanda.hosloproject.AlbumsAdapter;
import naifwanda.hosloproject.Audiovisual.BooksActivity;
import naifwanda.hosloproject.EncyclopediaActivity;
import naifwanda.hosloproject.Audiovisual.MoviesActivity;
import naifwanda.hosloproject.Audiovisual.MusicActivity;
import naifwanda.hosloproject.R;


/**
 * Fragment for managing the recyclerview of movies, music, books
 * code taken from: https://www.youtube.com/watch?v=7Fe1jigV5Qs
 *
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class CardFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private AlbumsAdapter adapter;
    private ArrayList<Album> albumList;


    public CardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_card, container);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        albumList = new ArrayList<>();
        adapter = new AlbumsAdapter(getContext(), albumList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();

        return view;
    }

     /**
     * Adding few albums for testing
      *
      * We check the context and show the cards according with that
     *
     */
    private void prepareAlbums() {
        if(MoviesActivity.class.isInstance(getActivity())) {
            int[] covers = new int[]{
                    R.drawable.chaplinfestival,
                    R.drawable.thebirds,
                    R.drawable.elbruto,
                    R.drawable.metropolis,
                    R.drawable.charade,
                    R.drawable.maniac,
                    R.drawable.whitezombie,
                    R.drawable.naifwanda


            };

            String info = getResources().getString(R.string.info_chaplin);
            String url = "https://archive.org/details/charlie_chaplin_film_fest";

            Album a = new Album("Charlie Chaplin Festival", "1938", covers[0], info, url);
            albumList.add(a);


            info = getResources().getString(R.string.info_birds);
            url = "https://archive.org/details/TheBirds1963AlfredHitchcock";

            a = new Album("The Birds", "1963", covers[1], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_bruto);
            url = "https://archive.org/details/El.bruto.1952.Luis.Bunuel";

            a = new Album("El Bruto", "1952", covers[2], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_metropolis);
            url = "https://archive.org/details/youtube-E1kxfiY_1DA";

            a = new Album("Metropolis", "1927", covers[3], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_charade);
            url = "https://archive.org/details/Charade19631280x696";

            a = new Album("Charade", "1963", covers[4], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_maniac);
            url = "https://archive.org/details/Maniac";

            a = new Album("Maniac", "1934", covers[5], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_zombie);
            url = "https://archive.org/details/VictorHalperinsWhiteZombie1932";

            a = new Album("White Zombie", "1932", covers[6], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_naif);
            url = "https://www.youtube.com/watch?v=syKTwJaI4HE";
            a = new Album("Yo naif", "2016", covers[7], info, url);
            albumList.add(a);


            adapter.notifyDataSetChanged();
        }else if (MusicActivity.class.isInstance(getActivity())){
            int[] covers = new int[]{
                    R.drawable.naifwanda,
                    R.drawable.costasfalto,
                    R.drawable.elbruto,
                    R.drawable.metropolis,
                    R.drawable.charade,
                    R.drawable.maniac,
                    R.drawable.whitezombie,
                    R.drawable.naifwanda


            };

            String info = getResources().getString(R.string.info_naifwanda);
            String url = "https://www.youtube.com/channel/UCSNHAltF4Ed0mJSz1Va8Zmw";

            Album a = new Album("Naifwanda", "X songs", covers[0], info, url);
            albumList.add(a);


            info = getResources().getString(R.string.info_costa);
            url = "https://www.youtube.com/channel/UCUCRrot2veUbsWjf_33W3Og";

            a = new Album("Costa Asfalto Beats", "X songs", covers[1], info, url);
            albumList.add(a);


            adapter.notifyDataSetChanged();
        }else if (BooksActivity.class.isInstance(getActivity())){

            int[] covers = new int[]{
                    R.drawable.raven,
                    R.drawable.quijote,
                    R.drawable.naifwanda,
                    R.drawable.metropolis,
                    R.drawable.charade,
                    R.drawable.maniac,
                    R.drawable.whitezombie,
                    R.drawable.naifwanda


            };

            String info = getResources().getString(R.string.info_raven);
            String url = "https://archive.org/details/ravenpoem00poegoog";

            Album a = new Album("The Raven", "1898", covers[0], info, url);
            albumList.add(a);


            info = getResources().getString(R.string.info_quijote);
            url = "https://archive.org/details/elingenio01cerv";

            a = new Album("El ingenioso hidalgo Don Quijote de la Mancha", "1883", covers[1], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_blognaif);
            url = "http://vaciandoelcargador.blogspot.no/";

            a = new Album("Vaciando el cargador [Blog]", "2012-Present", covers[2], info, url);
            albumList.add(a);

            adapter.notifyDataSetChanged();
        }else if (EncyclopediaActivity.class.isInstance(getActivity())){

            int[] covers = new int[]{
                    R.drawable.helsenorge,
                    R.drawable.helsedirektoratet,
                    R.drawable.nhn,
                    R.drawable.fhi,
                    R.drawable.ehelse,
                    R.drawable.lommelegen,
                    R.drawable.lvh,
                    R.drawable.naifwanda


            };

            String info = getResources().getString(R.string.info_helsenorge);
            String url = "https://helsenorge.no/";

            Album a = new Album("Helsenorge.no", "helsenorge.no", covers[0], info, url);
            albumList.add(a);


            info = getResources().getString(R.string.info_helsedir);
            url = "https://helsedirektoratet.no/English";

            a = new Album("Helsedirektoratet.no", "helsedirektoratet.no", covers[1], info, url);
            albumList.add(a);



            info = getResources().getString(R.string.info_nhn);
            url = "https://www.nhn.no/english/Pages/about.aspx";

            a = new Album("Norwegian Health Network", "nhn.no", covers[2], info, url);
            albumList.add(a);


            info = getResources().getString(R.string.info_fhi);
            url = "https://www.fhi.no/";

            a = new Album("Norwegian Institute of Public Health", "fhi.no", covers[3], info, url);
            albumList.add(a);



            info = getResources().getString(R.string.info_ehelse);
            url = "https://ehelse.no/";

            a = new Album("eHelse", "ehelse.no", covers[4], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_lommelegen);
            url = "http://www.lommelegen.no/";

            a = new Album("Lommelegen.no", "lommelegen.no", covers[5], info, url);
            albumList.add(a);

            info = getResources().getString(R.string.info_lvh);
            url = "http://lvh.no/";

            a = new Album("Legevakthåndboken", "lvh.no", covers[6], info, url);
            albumList.add(a);

            adapter.notifyDataSetChanged();
        }
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + user1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + user1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }








    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            RuntimeException e = new RuntimeException();
            try {
                throw e;
            } catch (RuntimeException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
