package naifwanda.hosloproject.Card;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import naifwanda.hosloproject.R;
import naifwanda.hosloproject.WebViewActivity;

public class CardInfoActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView cardImage;
    TextView cardTitle;
    TextView cardYear;
    TextView cardInfo;
    TextView cardUrl;
    Button buttonVisualize;
    String receivedUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_info);

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);

        //Back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        cardImage = (ImageView)findViewById(R.id.backdrop_image);
        cardTitle = (TextView)findViewById(R.id.txt_card_title);
        cardYear = (TextView)findViewById(R.id.txt_card_year);
        cardInfo = (TextView)findViewById(R.id.txt_card_info);
        //cardUrl = (TextView)findViewById(R.id.txt_card_url);
        buttonVisualize = (Button)findViewById(R.id.visualize);
        //cardUrl = (TextView) findViewById(R.id.)


        cardImage.setImageResource(getIntent().getIntExtra("image", 01));
        cardTitle.setText(getIntent().getStringExtra("title"));
        cardYear.setText(getIntent().getStringExtra("count"));
        cardInfo.setText(getIntent().getStringExtra("info"));
        receivedUrl = (String) getIntent().getStringExtra("url");

        //cardUrl.setText(getIntent().getStringExtra("url"));
        buttonVisualize.setOnClickListener(this);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                super.onBackPressed();
                /*Intent intent = new Intent(CardInfoActivity.this, AudiovisualMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.visualize:
                Intent intentToWebView = new Intent(this, WebViewActivity.class);
                intentToWebView.putExtra("url", receivedUrl);
                startActivity(intentToWebView);
                break;
            default:
                break;
        }
    }
}
